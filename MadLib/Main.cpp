//Tony Skoviak
//Dan Butzen
//Lydia Schmalz
//Tiffany Erdmann

#include <string>
#include <iostream>
#include <fstream>
#include <conio.h>

using namespace std;


void Print_MadLib(string* MadLib, ostream &os = cout);

int main()
{
	const int MADLIB_SIZE = 12;
	const string filepath = "C:\\Users\\Public\\MadLib.txt";
	string MadLib[MADLIB_SIZE] = { "a direction", "a direction", "a singular unit of time", "a noun", "A name", "a city", "a place of leisure",
								   "a verb ending in -ing", "a thing", "a plural noun", "an action", "a place" };

	for (int count = 0; count < MADLIB_SIZE; count++)
	{
		cout << "Please enter " << MadLib[count] << ": ";
		
		getline(cin, MadLib[count]);
	}

	Print_MadLib(MadLib);

	char save;
	cout << "Do you want to save your mad lib to a text file (Y/N)? ";
	cin >> save;

	if (save == 'Y' || save == 'y')
	{
		ofstream ofs;
		ofs.open(filepath);

		Print_MadLib(MadLib, ofs);

		ofs.close();
	}


	_getch();
	return 0;
}

void Print_MadLib(string* MadLib, ostream &os) {
	 os << "Now, this is a story all about how\n"
		<< "My life got flipped - turned " << MadLib[0] << "-side " << MadLib[1] << "\n"
		<< "And I'd like to take a " << MadLib[2] << "\n"
		<< "Just sit right there\n"
		<< "I'll tell you how I became the " << MadLib[3] << " of a town called " << MadLib[4] << "\n"
		<< "In west " << MadLib[5] << " born and raised\n"
		<< "On the " << MadLib[6] << " was where I spent most of my days\n"
		<< "Chillin' out maxin' relaxin' all cool\n"
		<< "And all " << MadLib[7] << " some " << MadLib[8] << " outside of the school\n"
		<< "When a couple of " << MadLib[9] << " who were up to " << MadLib[10] << "\n"
		<< "Started making trouble in my " << MadLib[11] << "\n"
		<< "I got in one little fight and my mom got scared \n"
		<< "She said 'You're movin' with your auntie and uncle in " << MadLib[4] << "'\n\n";
}